using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class playerScript: MonoBehaviour
{
    public float xMin = -10f;
    public float xMax = 10f;
    

    void Update()
    {
        float moveHorizontal = Input.GetAxis("Horizontal");


        Vector2 movement = new Vector2(moveHorizontal, 0);
        movement *= 15;

        transform.Translate(movement * Time.deltaTime);

        float xPosition = Mathf.Clamp(transform.position.x, xMin, xMax);
        
        transform.position = new Vector3(xPosition, transform.position.y, 0);
        
    }
}
