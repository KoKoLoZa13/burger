using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class enemies : MonoBehaviour
{

    int enemies_num = 5;
    
    public List<GameObject> enemyList = new List<GameObject>();
    Vector3 vectors = new Vector3(-8.0f, 2.0f, 0.0f);
    [SerializeField]
    public GameObject enemyPrefab;
    Rigidbody2D rb;


    void Start()
    {
        for (int i = 1; i <= enemies_num; i++)
        {
            GameObject enemy = Instantiate(enemyPrefab, vectors, Quaternion.identity);
            enemyList.Add(enemy);
            vectors.x += 4.0f;
            rb = enemy.AddComponent<Rigidbody2D>();
            rb.gravityScale = 0;
            BoxCollider2D boxCollider = enemy.AddComponent<BoxCollider2D>();
        }
    }

    // Update is called once per frame
    void Update()
    {
        foreach (GameObject enemy in enemyList)
        {
            Vector3 movement = new Vector3(15, 0, 0);
            rb = enemy.GetComponent<Rigidbody2D>();
            rb.AddForce(movement * 5 * Time.deltaTime);
            if (enemy.transform.position.x > 10.0f)
            {
                enemy.transform.position = new Vector3(-10, enemy.transform.position.y - 2.0f, 0);
            }
            

        }

    }
}
