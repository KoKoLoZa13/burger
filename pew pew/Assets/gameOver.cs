using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using TMPro;
using UnityEngine.SceneManagement;

public class gameOver : MonoBehaviour
{
    public enemies enemyScript;
    public shooting shootingScript;
    public UI UIScript;
    public playerScript playerMovements;
    List<GameObject> enemiesToRemove = new List<GameObject>();
    public Canvas canvas;
    public collidingBN collides;
    int myscore;

    void Start()
    {
        Debug.Log(collides.scores);
        Debug.Log(enemyScript.enemyList);
        Debug.Log(shootingScript.bulletList);
    }

    void Update()
    {
        foreach (GameObject enemy in enemyScript.enemyList)
        {
            if (enemy != null)
            {
                if (enemy.transform.position.y < -2.5)
                {
                    enemiesToRemove.Add(enemy);
                }
            }
        }
        if (enemiesToRemove.Count > 0)
        {
            myscore = collides.scores;
            MonoBehaviour[] scripts = FindObjectsOfType<MonoBehaviour>();

            foreach (MonoBehaviour script in scripts)
            {
                if (script != this)
                {
                    script.enabled = false;
                }
            }
            foreach (GameObject bullet in shootingScript.bulletList)
            {
                if (bullet != null)
                {
                    Destroy(bullet);
                }
            }
            foreach (GameObject enemy in enemyScript.enemyList)
            {
                Destroy(enemy);
            }

            GameObject textObject = new GameObject("Text");
            textObject.transform.SetParent(canvas.transform, false);
            TextMeshProUGUI textMeshProComponent = textObject.AddComponent<TextMeshProUGUI>();
            textMeshProComponent.text = System.String.Format("{0}{1}", "Game Over, Score: ", myscore);
            textMeshProComponent.alignment = TextAlignmentOptions.Center;
            RectTransform rectTransform = textObject.GetComponent<RectTransform>();
            rectTransform.anchoredPosition = new Vector2(0, 0);

            enemiesToRemove.Clear();

        }
    }
}
