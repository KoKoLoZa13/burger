using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class collidingBN : MonoBehaviour
{
    [SerializeField]
    public shooting shooting1;
    
    [SerializeField]
    public enemies enemies1;

    [SerializeField]
    public GameObject prefab;

    Rigidbody2D rb;
    public int scores;

    

    private List<GameObject> enemiesToRemove = new List<GameObject>();
    private List<GameObject> bulletsToRemove = new List<GameObject>();
    void Start()
    {
        Debug.Log(enemies1.enemyList);
        Debug.Log(shooting1.bulletList);
        Debug.Log(shooting1.position);
    }

  
    void Update()
    {
        DetectCollisions();
        RemoveCollidedGameObjects();
    }


    private void DetectCollisions()
    {
        foreach (GameObject enemy in enemies1.enemyList)
        {
            foreach (GameObject bullet in shooting1.bulletList)
            {
                if (IsCollision(enemy, bullet))
                {
                    enemiesToRemove.Add(enemy);
                    bulletsToRemove.Add(bullet);
                    scores += 1;
                }
            }
        }
    }

    private bool IsCollision(GameObject enemy, GameObject bullet)
    {
        float distance = Vector3.Distance(enemy.transform.position, bullet.transform.position);
        return distance <= 1.0f;
    }

    private void RemoveCollidedGameObjects()
    {
        foreach (GameObject enemy in enemiesToRemove)
        {
            enemies1.enemyList.Remove(enemy);
            Destroy(enemy);
        }
        
        foreach (GameObject i in enemiesToRemove) 
        {
            float randY = Random.Range(1f, 3f);
            float randX = Random.Range(-10f, 5f);
            Vector3 enemyPos = new Vector3(randX, randY, 0);
            GameObject someEnemy = Instantiate(prefab, enemyPos, Quaternion.identity);
            rb = someEnemy.AddComponent<Rigidbody2D>();
            rb.gravityScale = 0;
            
            enemies1.enemyList.Add(someEnemy);
        }
        enemiesToRemove.Clear();

        foreach (GameObject bullet in bulletsToRemove)
        {
            shooting1.bulletList.Remove(bullet);
            Destroy(bullet);
        }
        bulletsToRemove.Clear();
    }

}
