using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class shooting : MonoBehaviour
{
    [SerializeField]
    public GameObject bulletPrefab;

    [SerializeField]
    public List<GameObject> bulletList = new List<GameObject>();

    public float bulletSpeed = 20f;
    public Vector3 position;

    void Update()
    {
        if (Input.GetKeyDown(KeyCode.Space))
        {
            Vector3 position = new Vector3(transform.position.x, transform.position.y, 0.0f);
            GameObject bullet = Instantiate(bulletPrefab, position, Quaternion.identity);
            bulletList.Add(bullet);
            
        }

        if (bulletList.Count > 0)
        {
            foreach (GameObject bullet in bulletList)
            {
                Vector3 bulletPosition = bullet.transform.position;
                bulletPosition.y += 10 * Time.deltaTime; 
                bullet.transform.position = bulletPosition;
                
            }
        }
        if (bulletList.Count > 0)
        {
            for (int i = bulletList.Count - 1; i >= 0; i--)
            {
                GameObject bullet = bulletList[i];
                Vector3 bulletPosition = bullet.transform.position;
                bulletPosition.y += 10 * Time.deltaTime; 
                bullet.transform.position = bulletPosition;
                if (bullet.transform.position.y > 5)
                {
                    bulletList.RemoveAt(i);
                    Destroy(bullet);
                }
            }
        }
    }
}



