using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using TMPro;


public class UI : MonoBehaviour
{
    [SerializeField]
    TextMeshProUGUI score;

    public collidingBN collides;
    void Start()
    {
        Debug.Log(collides.scores);           
    }


    void Update()
    {
        score.text = System.String.Format("{0}{1}", "Score: ", collides.scores);
    }
}
